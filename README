Author: Richard Sharpe

A modification to rpcgen to also generate a Wireshark dissector from an 
XDR file.

This software is derived from the Ilumnos version of rpcgen.

At the moment the code is fairly rudimentary and does not have the
sort of nice stuff that the hand-coded dissectors have, like file-handle
snooping, file-handle hashes, conversion of time fields, conversion of
mode fields etc.

However, it can handle all the types in RFC1832, I believe.

BUILDING:

As always, 

autoreconf -vfi
configure
make

DEBUGGING CHANGES

Pass -d on the commandline below. That switches on debugging.

Add d_fprintf lines in the code if you want more debugging. Just like fprintf only
crunchy.

GENERATING A DISSECTOR FROM A SINGLE XDR FILE:

./src/wireshark_rpcgen -WN [-P "Proto description"] examples/nfsv3_xdr.x >& packet-nfsv3.c

You might want to preprocess the XDR file to pretty up thigs with something like:

   perl -e 'while (<>) { $_ =~ s/NFSPROC3_//; $_ =~ s/_PROGRAM//; print $_;}' < ./examples/nfsv3_xdr.x >& /tmp/nfsv3_clean_xdr.x

GENERATING A DISSECTOTR FROM MULTIPLE XDR FILES:

Suppose you have a protocol that uses multiple XDR files:

1. protocol1.x,
2. structs1.x, and
3. Structs2.x

First, create dissectors for the two structures:

  ./wireshark_rpcgen -WN -P "Structs1" -p structs1_diss structs1.xdr >& packet-structs1.c
  ./wireshark_rpcgen -WN -P "Structs2" -p structs2_diss structs2.xdr >& packet-structs2.c

Then, create a dissector for the protocol and pass in some important information:

  ./wireshark_rpcgen -WN protocol1.x >& packet-protocol.c

Sometimes your protocol will use a typedef that is defined in one of the type includes.
To avoid issues with the generator generating the wrong code use one or more -X
parameters to define the typedefs:

  ./wireshark_rpcgen -WN -X "typedef uint64_t some-funny_t;" protocol1.x \
                      >& packet-protocol.c

These typedefs must use the correct syntax for a typedef and are jammed into the
parser before any lines from the XDR file.

How you generate the list of structures is up to you but I suggest using Perl or
something like that. You must ensure that you include all of the structures that each
structure files knows how to dissect when you generate the protocol file.

BUILDING WIRESHARK WITH THE NEW CODE:

You should know how to buid Wireshark.

1. Place the new module(s) in epan/dissectors
2. Update epan/dissectors/Makefile.am at a minimum.
3. make

ISSUES

There is currently an issue with ENUMs which causes them to be listed incorrectly
in the dissection. The field name will show up as the type name instead of the 
field name. This is hard to fix with the way the generator is currently written
and I am looking at a rewrite to fix this issue.
