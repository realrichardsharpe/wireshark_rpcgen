/* rpc_wireshark.c  1.0  16/09/01
 *
 * Copyright Richard Sharpe, 2016
 *
 * rpc_wireshark.c, The actual code for generating the dissector in rpcgen.
 */

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <search.h>
#include <stdio.h>
#include <string.h>
#include "rpc/types.h"
#include "rpc_parse.h"
#include "rpc_util.h"
#include "rpc_wireshark.h"

static void write_wireshark_boilerplate(definition *def);
static void write_wireshark_proto_consts(void);
static void write_wireshark_forward_defs(definition *prog);
static void write_wireshark_ett_fields(definition *prog);
static void write_wireshark_hf_fields(definition *prog);
static void write_wireshark_enum_decls(definition *prog);
static void write_wireshark_structs_unions_dissect(definition *prog);
static void write_wireshark_struct_dissect(definition *prog, definition *st);
static void write_wireshark_union_dissect(definition *prog, definition *st);
static void write_wireshark_typedef_dissect(definition *def, definition *prog);
static void write_wireshark_typedefs_dissect(definition *prog);
static void write_wireshark_req_rsp_dissect(definition *prog);
static void write_wireshark_register_arrays(definition *prog);
static void write_wireshark_trailer(definition *prog);
static const char *is_base_type(const char *type, int *len, int *index);
static definition *resolve_type(const char *type, int try_defs);
static int args_has_void(decl_list *decls);

static typedef_list *typedefs = NULL;
static typedef_list **typedefs_end = &typedefs;
static enum_list *enums = NULL;
static enum_list **enums_end = &enums;
static int void_needed = 0;
//static void delete_hf_node(void *node);

void *hf_tree_root = NULL;

#define d_fprintf(...) \
  if (debug) \
    fprintf(__VA_ARGS__);

/*
 * We write out the dissector. There are two cases.
 *
 * 1. If the input file contains a program definition, use that to base
 *    the names or certain important functions, like registration etc.
 *
 *    Also, we might have to parse any include files to see what is in
 *    them.
 *
 * 2. If the input file does not contain a program, then use the path name
 *    for that. Take the last two components and sanitize them:
 *    - strip any final .x
 *    - change . to _
 */
void write_wireshark(const char *infilename)
{
  list *l;
  definition *def, *prog = NULL;
  definition fake_prog = {NULL, };

  /* Handle the prelude ... */
  for (l = defined; l != NULL; l = l->next)
    {
      typedef_list *list_elt;
      enum_list *elist_elt;
      version_list *v;
      proc_list *p;

      /* Collect some info we need */
      def = (definition *) l->val;
      switch (def->def_kind)
        {
          case DEF_PROGRAM:
          prog = def;
          // Find any void returns etc
          for (v = def->def.pr.versions; v != NULL; v = v->next)
            {
              for (p = v->procs; p != NULL; p = p->next)
                {
                  if (strcmp(p->res_type, "void") == 0)
                    {
                      void_needed = 1;
                      goto done_here;
                    }
                    // Need to search args
                    if (args_has_void(p->args.decls))
                      {
                        void_needed = 1;
                        goto done_here;
                      }
                }
            }
          done_here:
          break;

        case DEF_UNION:
          if (def->def.un.default_decl &&
              (strcmp(def->def.un.default_decl->type, "void") == 0))
            void_needed = 1;
          break;

        case DEF_ENUM:
          elist_elt = calloc(1, sizeof(enum_list));
          *enums_end = elist_elt;
          elist_elt->def = def;
          enums_end = &elist_elt->next;
          break;

        case DEF_TYPEDEF:
          list_elt = calloc(1, sizeof(typedef_list));
          *typedefs_end = list_elt;
          list_elt->def = def;
          typedefs_end = &list_elt->next;
          break;

        default:
            break;
      }
    }

  /*
   * If there is no program section, create a fake one. However, we need to
   * avoid registering a handoff dissector ...
   *
   * Also, force a fake prog section if told we don't want one.
   */
  if (!prog)
    {
      prog = &fake_prog;
      fake_prog.def_name = def_prog_name;
    }
  else if (no_program_section)
    {
      fake_prog.def_name = prog->def_name;
      fake_prog.def_kind = DEF_FAKE;
      prog = &fake_prog;
    }
  /*
   * Now, do each of the sections we need in turn.
   */
  write_wireshark_boilerplate (prog);

  write_wireshark_proto_consts();

  write_wireshark_forward_defs(prog);

  write_wireshark_ett_fields(prog);

  write_wireshark_hf_fields(prog);

  write_wireshark_enum_decls(prog);

  write_wireshark_structs_unions_dissect(prog);

  write_wireshark_typedefs_dissect(prog);

  if (prog->def_kind != DEF_FAKE)
    write_wireshark_req_rsp_dissect(prog);

  write_wireshark_register_arrays(prog);

  write_wireshark_trailer(prog);

}

static int args_has_void(decl_list *decls)
{
  for (; decls != NULL; decls = decls->next)
    {
      if (strcmp(decls->decl.type, "void") == 0)
        return 1;
    }

  return 0;
}

/*
 * Generate the boilerplate using the appropriate definition.
 * If the XDR file does not contain a program definition, then
 * the definition we are passed will be based on the file the
 * XDR comes from, and some entries are not required.
 */
static void write_wireshark_boilerplate(definition *def)
{
  fprintf(fout,
"/* packet-%s.c\n"
" * Routines for %s dissection\n"
" * Copyright %s, %s\n"
" *\n"
" * Wireshark - Network traffic analyzer\n"
" * By Gerald Combs <gerald@wireshark.org>\n"
" * Copyright 1998 Gerald Combs\n"
" *\n"
" * Portions copied shamelessly from packet-nfs.c\n"
" *\n"
" * This program is free software; you can redistribute it and/or\n"
" * modify it under the terms of the GNU General Public License\n"
" * as published by the Free Software Foundation; either version 2\n"
" * of the License, or (at your option) any later version.\n"
" *\n"
" * This program is distributed in the hope that it will be useful,\n"
" * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
" * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
" * GNU General Public License for more details.\n"
" *\n"
" * You should have received a copy of the GNU General Public License\n"
" * along with this program; if not, write to the Free Software\n"
" * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.\n"
" *\n"
" * GENERATED BY RPCGEN. DO NOT DO SERIOUS EDITS.\n"
" * NOTE, USES WIRESHARK 2.x APIs. MAY NEED CHANGES.\n"
" */\n"
"\n"
"#include \"config.h\"\n"
"\n"
"#include <stdio.h>\n"
"\n"
"#include <epan/packet.h>\n"
"#include <epan/prefs.h>\n"
"#include <epan/exceptions.h>\n"
"#include <epan/expert.h>\n"
"#include <epan/to_str.h>\n"
"#include <epan/decode_as.h>\n"
"#include <wsutil/crc16.h>\n"
"#include <wsutil/crc32.h>\n"
"#include \"packet-rpc.h\"\n"
"#include <math.h>\n"
"\n",
          def->def_name, def->def_name, "@COPYRIGHTDATE@", "@COPYRIGHTOWNER@");

  fprintf(fout,
"#define %s %s\n\n"
"void proto_register_%s(void);\n"
"void proto_reg_handoff_%s(void);\n\n",
          def->def_name, def->def.pr.prog_num,
	  def->def_name, def->def_name);

  /* TODO: Fix the year and pickup the copyright somehow. */
}

/*
 * Here we print out the protocol consts (DEF_PROTOCONST). We simpy
 * print the names for the moment, but may eventually want to parse them
 * so we can generate code to check for protocol violations.
 *
 * We also do not print anything other than #define blah blah for the moment
 * and may want to parse any include files to find out if any structs we do
 * not know about are in them so we can generate calls to external dissectors.
 *
 * Also print out all the const definitions as well.
 */
static void write_wireshark_proto_consts(void)
{
  list *l;
  definition *def;
  int proto_count = 0;

  for (l = defined; l != NULL; l = l->next)
    {
      def = (definition *) l->val;
      switch (def->def_kind) {
        case DEF_PROTOCONST:
	  /* This strips %#include and passes %#define ... */
          if (strncmp("#include ", def->def_name, 8))
            fprintf(fout, "%s\n", def->def_name);
          proto_count++;
	  break;

        case DEF_CONST:
          fprintf(fout, "#define %s %s\n", def->def_name, def->def.co);
          proto_count++;
          break;

        default:
          break;
      }
    }

  if (proto_count)
    fprintf(fout, "\n");
}

/*
 * Lookup a type. If it is a base type or opaque or string, return that.
 * Otherwise look it up in the list of defintions.
 */
const char *lookup_type(const char *type)
{
  list *l;
  definition *def;
  int len = 0, index = 0;

  if (is_base_type(type, &len, &index))
    return type;

  if (strcmp(type, "string") == 0 || strcmp(type, "opaque") == 0)
    return type;

  for (l = defined; l != NULL; l = l->next)
    {
      def = (definition *) l->val;

      if (strcmp(def->def_name, type) == 0)
        {
          return def->def_name;
        }
    }

  return NULL;
}

/*
 * Generate forward defs for all the dissection routines we are generating.
 */
static void write_wireshark_forward_defs(definition *prog)
{
  list *l;
  definition *def;

  /*
   * If there is no program definition, the structure names
   * do not need qualification as they have to be unique anyway.
  */

  for (l = defined; l != NULL; l = l->next)
    {
      version_list *v = NULL;
      proc_list *p = NULL;
      decl_list *d = NULL;
      definition *target;

      def = (definition *) l->val;
      switch (def->def_kind) {
        case DEF_TYPEDEF:
          d_fprintf(fout, "//TypeDef: Name: %s, old_type: %s\n",
                    def->def_name, def->def.ty.old_type);
          /*
           * For those typedefs that map to a list we need one of these
           */
          target = resolve_type(def->def.ty.old_type, 1);
          if (prog->def_kind == DEF_FAKE &&
              target != NULL && target->def_kind == DEF_STRUCT &&
              def->def.ty.rel == REL_ARRAY)
            {
                  fprintf(fout, "int dissect_%s(tvbuff_t *tvb, packet_info *pinfo, "
                                "proto_tree *tree, int offset, const char *label);\n",
                          def->def_name);
            }
          else if (!lookup_type(def->def.ty.old_type))
            {
              fprintf(fout, "int dissect_%s(tvbuff_t *tvb, packet_info *pinfo, "
                            "proto_tree *tree, int offset, const char *label);\n",
                      def->def.ty.old_type);

            }
          break;

        case DEF_STRUCT:
        case DEF_UNION:
          fprintf(fout, "%sint dissect_%s(tvbuff_t *tvb, packet_info *pinfo, "
                        "proto_tree *tree, int offset, const char *label);\n",
                  (prog->def_kind != DEF_FAKE ? "static " : ""), def->def_name);

          /*
           * Now go through the decls and if we cannot find the type
           * then declare a dissector for that.
           */
          for (d = def->def.st.decls; d != NULL; d = d->next)
            {
              if (!lookup_type(d->decl.type))
                {
                  fprintf(fout, "int dissect_%s(tvbuff_t *tvb, packet_info *pinfo, "
                                "proto_tree *tree, int offset, const char *label);\n",
                          d->decl.type);
                }
            }
          break;

        case DEF_PROGRAM:
          /*
           * If the return type is not a base type and is not defined
           * then we need to generate a forward declaration for it.
           * This is only used for the case where definitions are
           * split across multiple XDR files. This is a bit of a hack because
           * there are more types that might be undefined.
           */

          /*
           * Don't want these if not asked for.
           */
          if (prog->def_kind == DEF_FAKE)
            break;

          for (v = def->def.pr.versions; v != NULL; v = v->next)
            {
              for (p = v->procs; p != NULL; p = p->next)
                {
                  const char * tplt = "static int\ndissect_%s%s_%s("
                    "tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, "
                    "void *data);\n";
                  fprintf(fout, tplt, p->proc_name, v->vers_num, "call");
                  fprintf(fout, tplt, p->proc_name, v->vers_num, "reply");

                 if (!lookup_type(p->res_type))
                   {
                     fprintf(fout, "int dissect_%s(tvbuff_t *tvb, packet_info *pinfo, "
                                   "proto_tree *tree, int offset, const char *label);\n",
                             p->res_type);
                   }

                /* Also need to handle the arguments */
                for (d = p->args.decls; d != NULL; d = d->next)
                  {
                    if (!lookup_type(d->decl.type))
                      fprintf(fout, "int dissect_%s(tvbuff_t *tvb, packet_info *pinfo, "
                                    "proto_tree *tree, int offset, const char *label);\n",
                              d->decl.type);
                  }
                }
            }
          break;

        default:
          break;
      }
    }

  fprintf(fout, "\n");
}

/*
 * We create all the required hf fields. Because many of them are the same,
 * eg hf_status is only needed once, we store the names in a binary tree
 * to ensure we only output an hf field once. We actually use it in a
 * special way later on as well. Also used for ett fields.
 */

struct hf_tree_info {
	char name[1024];  /* The name of the field                   */
	bool printed;     /* Whether we have printed the init or not */
};

#if 0
static void delete_hf_node(void *node)
{
  free(node);
}
#endif

static int compare_hf_nodes(const void *pa, const void *pb)
{
  const struct hf_tree_info *hfa = pa, *hfb = pb;

  return strcmp(hfa->name, hfb->name);
}

/*
 * Generate ett_xyz variables for all the structure-like things in the proto.
 */
#define check_and_print_ett(...) \
  do { \
    void *fp = NULL; \
    struct hf_tree_info *ep = calloc(sizeof(struct hf_tree_info), 1); \
    \
    if (ep == NULL) \
      perror("unable to get memory for ett name"); \
    snprintf(ep->name, sizeof(ep->name), __VA_ARGS__); \
    \
    if ((fp = tfind((void *)ep, &hf_tree_root, compare_hf_nodes)) == NULL) { \
      if (tsearch((void *)ep, &hf_tree_root, compare_hf_nodes) == NULL) { \
        perror("Error inserting ett name into hash table"); \
      } \
      fprintf(fout, "static gint %s = -1;\n", ep->name); \
    } else { \
    } \
  } while (0);

static void handle_ett_array_vec(char *name, declaration *decl)
{
  definition *new_def = NULL;
  relation rel = decl->rel;
  char *type = decl->type;

  new_def = resolve_type(type, 0);

  /*
   * Here, if the source is not an array, then check the dest.
   */
  if (new_def)
    {
      if (rel != REL_VECTOR && rel != REL_ARRAY)
        rel = new_def->def.ty.rel;
      type = new_def->def.ty.old_type;
    }

  d_fprintf(fout, "//ETT: rel: %d, new_def: %p\n", decl->rel, (void *)new_def);
  d_fprintf(fout, "//ETT: new rel: %d, type: %s, name: %s\n",
            rel, type, name);

  /*
   * We don't need an ett if it is an opaque or string vector
   */
  if ((rel == REL_VECTOR &&
       (strcmp(type, "opaque") == 0 || strcmp(type, "string") == 0)) ||
      rel == REL_ARRAY)
    {
      check_and_print_ett("ett_%s_%s", name, decl->name);
    }
  else if (rel == REL_POINTER)
    {
      check_and_print_ett("ett_%s", decl->type);
      check_and_print_ett("ett_%s_item", decl->type);
    }
  else {
    /* We need these for structs in some cases ... */
    check_and_print_ett("ett_%s_%s", name, decl->name);
  }
}

static void write_wireshark_ett_fields (definition *prog)
{
  list *l;
  definition *def;

  if (prog)
    check_and_print_ett("ett_%s", prog->def_name);

  for (l = defined; l != NULL; l = l->next)
    {
      version_list *v = NULL;
      proc_list *p = NULL;
      decl_list *d = NULL;
      case_list *c = NULL;
      definition *target;

      def = (definition *) l->val;
      switch (def->def_kind) {
        case DEF_STRUCT:
          check_and_print_ett("ett_%s", def->def_name);
          // Go through the decls looking for Vectors or Arrays
         for (d = def->def.st.decls; d != NULL; d = d->next)
           {
             handle_ett_array_vec(def->def_name, &d->decl);
           }
           break;

        case DEF_UNION:
          check_and_print_ett("ett_%s", def->def_name);

          /*
           * Now, just like the above, we have to go through the various
           * cases and the default and check if we have to insert any ETT fields.
           */
          if (def->def.un.default_decl) /* Only if it exists. */
            handle_ett_array_vec(def->def_name, def->def.un.default_decl);

          for (c = def->def.un.cases; c != NULL; c = c->next)
            {
              if (!c->contflag) /* Only if not a continued case */
                handle_ett_array_vec(c->case_decl.type, &c->case_decl);
            }

          break;

        case DEF_TYPEDEF:
          /*
           * For those typedefs that map to a list we need one of these
           */
          target = resolve_type(def->def.ty.old_type, 1);
          if (prog->def_kind == DEF_FAKE &&
              target != NULL && target->def_kind == DEF_STRUCT &&
              def->def.ty.rel == REL_ARRAY)
            {
              check_and_print_ett("ett_%s", def->def_name);
            }

          break;

        /* Unfortunately, we need some of these */
        case DEF_PROGRAM:
          for (v = def->def.pr.versions; v != NULL; v = v->next)
            {
              for (p = v->procs; p != NULL; p = p->next)
                {
                  for (d = p->args.decls; d != NULL; d = d->next)
                    {
                      handle_ett_array_vec(p->args.argname, &d->decl);
                    }
                }
            }
          break;

        default:
          break;
        }
    }

  fprintf(fout, "\n");
}

/*
 * Generate all the hf_xyz variables needed.
 */
#define check_and_print_hf(...) \
  do { \
    struct hf_tree_info *sp, *ep = calloc(sizeof(struct hf_tree_info), 1); \
    \
    if (ep == NULL) \
      perror("unable to get memory"); \
    snprintf(ep->name, sizeof(ep->name), __VA_ARGS__); \
    \
    if ((sp = tfind((void *)ep, &hf_tree_root, compare_hf_nodes)) == NULL) { \
      if (tsearch((void *)ep, &hf_tree_root, compare_hf_nodes) == NULL) { \
        perror("Error inserting into hash table"); \
      } \
      fprintf(fout, "static gint %s = -1;\n", ep->name); \
    } \
  } while (0);

static void
gen_hf_for_decl(char *name, declaration *decl)
{
  definition *new_def = NULL;
  relation rel = decl->rel;
  char *type = decl->type;
  int len = 0, index = 0;

  new_def = resolve_type(type, 0);
  if (new_def)
    {
      d_fprintf(fout, "//newDef: old_type: %s, array_max: %s, rel: %d\n",
                new_def->def.ty.old_type, new_def->def.ty.array_max,
                new_def->def.ty.rel);

      /*
       * Only update if the original type was not an array
       */
      if (rel != REL_VECTOR && rel != REL_ARRAY)
        rel  = new_def->def.ty.rel;
      type = new_def->def.ty.old_type;
    }

  d_fprintf(fout, "//defN: %s, T: %s, N: %s\n",
            name, type, decl->name);

  /*
   * If it's void, forget it, because we have one of those.
   */
  if (strcmp(type, "void") == 0)
    return;

  /*
   * field names must be qualified by the parent
   * because there could be two fields with the same
   * name in different structures.
   *
   * However, if it refers to a structure, or we don't know
   * it's type we don't need an hf field. If we don't know its
   * type, we assume some other module does.
   *
   * When creating the name, use the original type as well.
   */
  if (rel == REL_ARRAY)
    {
      check_and_print_hf("hf_%s_%s_len", name, decl->name);

      /*
       * If it's a base type, or an opaque or string we need this,
       * but not otherwise.
       */
      if (is_base_type(type, &len, &index) ||
           strcmp(type, "opaque") == 0 ||
           strcmp(type, "string") == 0)
         check_and_print_hf("hf_%s_%s_val", name, decl->name);
    }
  else if (rel == REL_VECTOR)
    {
      if (is_base_type(type, &len, &index) ||
          strcmp(type, "opaque") == 0 ||
          strcmp(type, "string") == 0)
        check_and_print_hf("hf_%s_%s_val", name, decl->name);
    }
  else if (rel == REL_POINTER)
    {
      check_and_print_hf("hf_value_follows");
    }
  else if (is_base_type(type, &len, &index))
    {
      d_fprintf(fout, "//isBaseType: %s, T: %s, N: %s\n",
                name, type, decl->name);
      check_and_print_hf("hf_%s_%s", name, decl->name);
    }
}

static void write_wireshark_hf_fields (definition *prog)
{
  list *l;
  definition *def;

  if (prog)
    fprintf(fout, "static gint proto_%s = -1;\n", prog->def_name);

  if (void_needed)
    fprintf(fout, "static gint hf_void = -1;\n");

  for (l = defined; l != NULL; l = l->next)
    {
      decl_list *d = NULL;
      case_list *c = NULL;
      version_list *v = NULL;
      proc_list *p = NULL;
      definition *target;

      def = (definition *) l->val;
      switch (def->def_kind)
        {
          case DEF_STRUCT:
            for (d = def->def.st.decls; d != NULL; d = d->next)
              {
                gen_hf_for_decl(def->def_name, &d->decl);
              }
            break;

          case DEF_TYPEDEF:
          /*
           * For those typedefs that map to a list we need one of these
           */
          target = resolve_type(def->def.ty.old_type, 1);
          if (prog->def_kind == DEF_FAKE &&
              target != NULL && target->def_kind == DEF_STRUCT &&
              def->def.ty.rel == REL_ARRAY)
            {
              check_and_print_hf("hf_%s_len", def->def_name);
            }
            break;

          case DEF_ENUM:
            check_and_print_hf("hf_%s", def->def_name);
            break;

          case DEF_UNION:
            check_and_print_hf("hf_%s", def->def.un.enum_decl.type);
            if (def->def.un.default_decl) /* Only if there is a default */
              gen_hf_for_decl(def->def_name, def->def.un.default_decl);
            for (c = def->def.un.cases; c != NULL; c = c->next)
              {
                if (!c->contflag) /* Only if it is not a contflag */
                  gen_hf_for_decl(def->def_name, &c->case_decl);
              }
            break;

          case DEF_PROGRAM:
            for (v = def->def.pr.versions; v != NULL; v = v->next)
              {
                // hf fields for the versions
                check_and_print_hf("hf_%s_procedure_v%s",
                        prog->def_name, v->vers_num);

                for (p = v->procs; p != NULL; p = p->next)
                  {
                    definition *ret_type;
                    d_fprintf(fout, "//PROC: N: %s, arg#: %d, an: %s, res: %s\n",
                              p->proc_name, p->arg_num, p->args.argname,
                              p->res_type);

                    ret_type = resolve_type(p->res_type, TRUE);
                    if (ret_type)
                      {
                        d_fprintf(fout, "//retType: Name: %s, kind: %d\n",
                                  ret_type->def_name, ret_type->def_kind);
                      }
                    else
                      {
                        int len = 0, index = 0;

                        if (strcmp(p->res_type, "void") != 0 &&
                            is_base_type(p->res_type, &len, &index))
                          check_and_print_hf("hf_%s_%s", p->proc_name, p->res_type);
                      }

                    for (d = p->args.decls; d != NULL; d = d->next)
                      {
                        d_fprintf(fout, "//ARG: N: %s, T: %s...\n",
                                  d->decl.name, d->decl.type);
                        gen_hf_for_decl(p->args.argname, &d->decl);
                      }
                  }
              }
            break;

          default:
            break;
        }
    }

  fprintf(fout, "\n");
}

static void write_wireshark_enum_decls (definition *prog)
{

  list *l;
  definition *def;

  for (l = defined; l != NULL; l = l->next)
    {
      def = (definition *) l->val;

      if (def->def_kind == DEF_ENUM)
        {
          enumval_list *e = NULL;

          /*
           * Print out some #defines first, then the enum def. The defines
           * are needed in case a union uses them.
           */
          for (e = def->def.en.vals; e != NULL; e = e->next)
            {
              fprintf(fout, "#define %s %s\n", e->name, e->assignment);
            }

          fprintf(fout, "\n");

          fprintf(fout, "static const value_string %s_vals[] = {\n",
                  def->def_name);

          for (e = def->def.en.vals; e != NULL; e = e->next)
            {
              fprintf(fout, "\t{ %s, \"%s\" },\n", e->assignment, e->name);
            }

          fprintf(fout, "\t{ 0, NULL }\n};\n\n");
        }
    }
}

static void write_wireshark_structs_unions_dissect (definition *prog)
{
  list *l;
  definition *def;

  for (l = defined; l != NULL; l = l->next)
    {
      def = (definition *) l->val;

      switch (def->def_kind)
      {
        case DEF_STRUCT:
          write_wireshark_struct_dissect (prog, def);
          break;

        case DEF_UNION:
          write_wireshark_union_dissect (prog, def);
          break;

        default:
          break;
      }
    }
}

/*
 * Map the incoming types to base types if we have them ...
 *
 * We deal with opaque and string separately. Min size is 4 bytes, as per
 * the spec. Will have to add float types.
 *
 * The first array must be kept sorted! They have to be kept in sync.
 *
 * 'void' is a known type, but occupies no space and is dealt with
 * specially in the code.
 */
static const char *known_types[] = {
	"bool", "char", "int", "long", "quad_t", "short", "longlong_t",
        "u_char", "u_int", "u_long", "u_longlong_t", "u_quad_t", "u_short",
        "int32_t", "uint32_t", "int64_t", "uint64_t", "void", "unsigned int"
};

static const char *rpc_types[] = { "FT_INT32", "FT_INT32", "FT_INT32",
        "FT_INT32", "FT_INT64", "FT_INT32", "FT_INT64", "FT_UINT32",
        "FT_UINT32", "FT_UINT32", "FT_UINT64", "FT_UINT64", "FT_INT32",
        "FT_UINT32", "FT_UINT32", "FT_INT64", "FT_UINT64", "FT_UINT32",
        "FT_UINT32"
};

static const int type_sizes[] = { 4, 4, 4, 4, 8, 4, 8, 4, 4, 4, 8, 8, 4, 4,
                                  4, 8, 8, 4, 4 };

static const char *var_types[] = {
	"guint32", "guint32", "guint32", "guint32", "guint64", "guint32",
        "guint64", "guint32", "guint32", "guint32", "guint64", "guint64",
        "guint32", "guint32", "guint32", "guint64", "guint64", "guint32",
        "guint32"
};

static const char *access_names[] = {
        "ntohl", "ntohl", "ntohl", "ntohl", "ntoh64", "ntohl", "ntoh64",
	"ntohl", "ntohl", "ntohl", "ntoh64", "ntoh64", "ntohl", "ntohl",
        "ntohl", "ntoh64", "ntoh64", "ntohl", "ntohl"
};

static const char *proto_item_names[] = {
        "int", "int", "int", "int", "int64", "int", "int64", "uint", "uint",
	"uint", "uint64", "uint64", "int", "uint", "uint", "int64", "uint64",
        "uint", "uint"
};

/*
 * Figure out if this is a known type and return its definition.
 *
 * Typedefs are used for lists etc, so unless asked, only look up
 * in typedefs list.
 */
static definition *resolve_type(const char *type, int try_defs)
{
  definition *resolved = NULL;
  typedef_list *l = typedefs;

  /* Need to keep trying until cannot resolve again */
  while (l)
    {
      if (strcmp(type, l->def->def_name) == 0)
        {
          resolved = l->def;
          break;
        }
      l = l->next;
    }

  if (!try_defs)
    return resolved;

  // If not found, check all the definitions
  // Generalize this ...
  if (resolved == NULL)
    {
      list *l;
      definition *def;

      for (l = defined; l != NULL; l = l->next)
        {
          def = (definition *) l->val;
          if ((def->def_kind == DEF_ENUM ||
               def->def_kind == DEF_STRUCT) &&
              (strcmp(def->def_name, type) == 0))
            return def;
        }
    }

  return resolved;
}

/*
 * Resolve an enum from the name
 */
static definition *resolve_enum(const char *type)
{
  definition *resolved = NULL;
  enum_list *l = enums;

  while (l)
    {
      if (strcmp(type, l->def->def_name) == 0)
        {
          resolved = l->def;
          break;
        }
      l = l->next;
    }

  return resolved;
}

/*
 * Figure out if it is a base type, even if it is typedef'd.
 * Also, return the index into the known_types array if it is.
 */
static const char *is_base_type(const char *type, int *len, int *index)
{
  definition *def = resolve_type(type, 0);
  int array_size = sizeof(known_types)/sizeof(char *), i;
  const char *ret = NULL;

  *len = 0; *index = -1;
  if (def)
    {
      switch (def->def_kind)
        {
          case DEF_TYPEDEF:
            type = def->def.ty.old_type;
            break;

          case DEF_ENUM:
            return NULL; // Not a base type
            break;

          default:
            break;
        }
    }

  for (i = 0; i < array_size; i++)
    {
      if (strcmp(type, known_types[i]) == 0)
        {
          *len = type_sizes[i];
          *index = i;
          ret = type;
          break;
        }
    }

  return ret;
}

/*
 * Allocates space to contain a label. If it is an alias, just copy the
 * name. If an array or a vector, use name<[n]> or name[N].
 */
char *resolve_field_label(struct declaration *decl)
{
  int len;
  char *res = NULL;

  len = strlen(decl->name) + 1;

  switch (decl->rel)
    {
      case REL_VECTOR:
        len = len + 2 + strlen(decl->array_max);
        res = malloc(len);
        snprintf(res, len, "%s[%s]", decl->name, decl->array_max);
        break;

      case REL_ARRAY:
        len = len + 2 + strlen(decl->array_max);
        res = malloc(len);
        snprintf(res, len, "%s<%s>", decl->name, decl->array_max);
        break;

      case REL_POINTER:
      case REL_ALIAS:
        res = malloc(len);
        strcpy(res, decl->name);
        break;

      default:
        break;
    }

  return res;
}

/*
 * @param use_parent is used to tell us whether to use the type name in
 * constructing ett_XXX names. When a type has been morphed into its underlying
 * type we pass in the original type name in the parent name where it is not
 * otherwise used.
 */
static void write_wireshark_decl(declaration *decl, const char *parent_name,
                                 char *ctx, char *indent, bool use_parent)
{
  switch (decl->rel)
    {
      int len = 0, index = 0;
      definition *def = NULL;

      case REL_VECTOR:
	  /*
           * Generate each of the items. If they are simple types,
           * then just insert them, but if not, then will have to dissect
           * them.
           *
           * The basic unit is 4 bytes, unless a string or opaque, but
           * even then, they still round up to 4 bytes.
           *
           * A vector of opaque bytes is different, however, as is
           * a vector of bytes. Strings can only be arrays. No need
           * to deal with them here.
           */
          d_fprintf(fout, "//V, P=%s T=%s N=%s L=%s, use_parent: %d\n",
                    decl->prefix, decl->type, decl->name, decl->array_max,
                    use_parent);
          if (strcmp(decl->type, "opaque") == 0)
            {
              // If it is opaque, just pull off that many bytes.
              fprintf(fout, "%sproto_tree_add_item(%s, hf_%s_%s_val, tvb, "
                            "offset, %s, ENC_BIG_ENDIAN);\n",
                            indent, ctx, use_parent ? parent_name : decl->type, \
			    decl->name, decl->array_max);
              fprintf(fout, "%soffset += %s;\n", indent, decl->array_max);
              fprintf(fout, "%sif (%s %% 4) offset += 4 - (%s %% 4);\n",
                      indent, decl->array_max, decl->array_max); // Roundup
              return;
            }

          /* Insert a subtree named by this items name */
          fprintf(fout, "%sst_xtra = proto_tree_add_subtree(tree, tvb, offset, -1, "
                        "ett_%s_%s, &it_xtra, \"%s[%s]\");\n\n",
                  indent, parent_name, decl->name, decl->name, decl->array_max);

          ctx = (char *)"st_xtra";

          fprintf(fout, "%sold_offset_xtra = offset;\n", indent);
          fprintf(fout, "%sfor (i = 0; i < %s; i++) {\n",
                  indent, decl->array_max);
          if (is_base_type(decl->type, &len, &index))
            {
              fprintf(fout, "%s\t%s val;\n\n", indent, var_types[index]);
              fprintf(fout, "%s\tval = tvb_get_%s(tvb, offset);\n",
                      indent, access_names[index]);
              fprintf(fout, "%s\tproto_tree_add_%s_format(%s, hf_%s_%s, "
                            "tvb, offset, %d, val, \"%s[%%d]\", i);\n",
                            indent, proto_item_names[index], ctx, parent_name,
                            decl->name, len, decl->name);
              fprintf(fout, "%s\toffset += %d\n", indent, type_sizes[index]);
            }
          else
            {
              fprintf(fout, "%s\tchar decl_label[%lu];\n\n",
                      indent, strlen(decl->name) + 3 + strlen(decl->array_max));
              fprintf(fout, "%s\tsprintf(decl_label, \"%s[%%d]\",i);\n",
                      indent, decl->name);
              fprintf(fout, "%s\toffset = dissect_%s(tvb, pinfo, %s, offset, "
                            "decl_label);\n",
                      indent, decl->type, ctx);
            }
          fprintf(fout, "\n");
          fprintf(fout, "%s}\n\n", indent);
          fprintf(fout, "%sproto_item_set_len(it_xtra, offset - old_offset_xtra);\n",
                  indent);
          break;

      case REL_ARRAY:
          /* if it's a base type (including via typedef, then generate
           * direct code, otherwise, call a separate dissect function
           *
           * This is for variable-length arrays, but string and opaque are
           * slightly different. However, they still need a subtree because
           * they have two components, a length and a value.
           */

          d_fprintf(fout, "//A, P=%s T=%s N=%s L=%s Parent=%s,UP:%d\n", decl->prefix,
                    decl->type, decl->name, decl->array_max, parent_name, use_parent);

          fprintf(fout, "%sold_offset_xtra = offset;\n", indent);

          // Insert a subtree for the item. Types will be unique.
          fprintf(fout, "%sst_xtra = proto_tree_add_subtree(%s, "
                        "tvb, offset, -1, ett_%s_%s, &it_xtra, \"%s<>\");\n\n",
                  indent, ctx, use_parent ? parent_name : decl->type,
                  decl->name, decl->name);

          if (strcmp(decl->type, "opaque") == 0 ||
              strcmp(decl->type, "string") == 0)
            {
              fprintf(fout, "%slen = tvb_get_ntohl(tvb, offset);\n", indent);
              fprintf(fout, "%s\tproto_tree_add_uint(st_xtra, hf_%s_%s_len, tvb, "
                            "offset, 4, len);\n",
                      indent, use_parent ? parent_name : decl->type, decl->name);
              fprintf(fout, "%s\toffset += 4;\n\n", indent);

              fprintf(fout, "%sif (len) {\n", indent);
              fprintf(fout, "%s\tproto_tree_add_item(st_xtra, hf_%s_%s_val, tvb, "
                            "offset, len, ENC_BIG_ENDIAN);\n",
                      indent,
                      use_parent ? parent_name : decl->type, decl->name);
              fprintf(fout, "%s\toffset += len;\n", indent);
              fprintf(fout, "%s\tif (len %% 4) offset += 4 - (len %% 4);\n", indent); // Roundup
              fprintf(fout, "%s}\n", indent);
              fprintf(fout, "%sproto_item_set_len(it_xtra, "
                            "offset - old_offset_xtra);\n\n", indent);
              return;
            }

          // Insert code to get the length
          fprintf(fout, "%slen = tvb_get_ntohl(tvb, offset);\n", indent);
          fprintf(fout, "%sproto_tree_add_uint(st_xtra, hf_%s_%s_len, tvb, "
                        "offset, 4, len);\n",
                  indent, use_parent ? parent_name : decl->type, decl->name);
          fprintf(fout, "%soffset += 4;\n\n", indent);

          /* Now handle the ones we need to iterate across */
          fprintf(fout, "%sfor (i = 0; i < len; i++) {\n", indent);

          if (is_base_type(decl->type, &len, &index))
            {
              fprintf(fout, "%s\t%s val;\n\n", indent, var_types[index]);
              fprintf(fout, "%s\tval = tvb_get_%s(tvb, offset);\n",
                      indent, access_names[index]);
              fprintf(fout, "%s\tproto_tree_add_%s_format(st_xtra, hf_%s_%s_val, "
                            "tvb, offset, %d, val, \"%s_val[%%d]\", i);\n",
                            indent, proto_item_names[index],
                            use_parent ? parent_name : decl->type, decl->name,
			    len, decl->name);
              fprintf(fout, "%s\toffset += %d;\n", indent, type_sizes[index]);
            }
          else
            {
              fprintf(fout, "%s\tchar decl_label[%lu];\n\n",
                      indent, strlen(decl->name) + 3 + 4 + strlen(decl->array_max));
              fprintf(fout, "%s\tsprintf(decl_label, \"%s_val[%%d]\",i);\n",
                      indent, decl->name);
              fprintf(fout, "%s\toffset = dissect_%s(tvb, pinfo, st_xtra, "
                            "offset, decl_label);\n",
                      indent, decl->type);
            }
          fprintf(fout, "%s}\n\n", indent);
          fprintf(fout, "%sproto_item_set_len(it_xtra, "
                        "offset - old_offset_xtra);\n\n", indent);
	  break;

      case REL_POINTER:
          /*
           * These are invariably used for self-referencing structures that
           * use tail recursion. We hide that in the structure dissector.
           */
          d_fprintf(fout, "//P, P=%s, T=%s, N=%s\n", decl->prefix, decl->type,
                    decl->name);
          fprintf(fout, "%soffset = dissect_%s(tvb, pinfo, %s, offset, "
                        "\"%s<>\");\n\n",
                  indent, decl->type, ctx, decl->name);
          break;

      case REL_ALIAS:
          // If it is a direct base type, or resolves to a direct base type
          // we can handle it here. However, if it resolves to a vector or
          // an array, we have to send it back through.
          d_fprintf(fout, "//AL: P=%s, T=%s, N=%s, Parent=%s, use_parent: %d\n",
		    decl->prefix,  decl->type, decl->name,
                    parent_name, use_parent);
          if ((def = resolve_enum(decl->type)))
            {
              d_fprintf(fout, "//Adding enum ... %s\n", def->def_name);
              fprintf(fout, "%sproto_tree_add_item(%s, hf_%s, tvb, "
  		            "offset, 4, ENC_BIG_ENDIAN);\n",
  	              indent, ctx, def->def_name);
              fprintf(fout, "%soffset += 4;\n\n", indent);

            }
          else if (is_base_type(decl->type, &len, &index))
            {
              // Handle void first. Nothing to fetch from the TVB.
              if (strcmp(decl->type, "void") == 0)
                {
                  fprintf(fout, "%sproto_tree_add_string(%s, "
                                "hf_void, tvb, 0, 0, \"\");\n",
                          indent, ctx);
                }
              else
                {
		  /*
		   * Here we do not need to qualify the name, just
		   * use the name as is when generating the hf structure.
		   */
                  d_fprintf(fout, "//Adding non-void base type ... %s_%s\n",
                            parent_name, decl->name);
	          fprintf(fout, "%sproto_tree_add_item(%s, hf_%s_%s, "
                                "tvb, offset, %d, ENC_BIG_ENDIAN);\n",
                          indent, ctx, use_parent ? parent_name : decl->type,
			  decl->name, len);
                  fprintf(fout, "%soffset += %d;\n", indent, len);
                }
            }
          else
            {
              definition *def;
              // It might be a typedef to an array, or something defined
              // in another XDR file ... generate the correct stuff.
              // Here, it cannot be a vector or array but it might be
              // an enum.
              def = resolve_type(decl->type, 1);
              // HACK ALERT ... fix!
              if (def && def->def_kind != DEF_ENUM && def->def_kind != DEF_STRUCT)
                {
                  struct declaration new_decl;

                  new_decl.name      = decl->name;
                  new_decl.prefix    = def->def.ty.old_prefix;
                  new_decl.type      = def->def.ty.old_type;
                  new_decl.rel       = def->def.ty.rel;
                  new_decl.array_max = def->def.ty.array_max;
                  d_fprintf(fout, "// handling morphed type: T: %s, N: %s, P: %s\n",
                            decl->type, new_decl.name, parent_name);
                  write_wireshark_decl(&new_decl, parent_name, ctx, indent, true);
                }
              else
                {
		  char *label = resolve_field_label(decl);
                  d_fprintf(fout, "// handling struct and non-union types\n");
                  fprintf(fout, "%soffset = dissect_%s(tvb, pinfo, %s, "
                                "offset, \"%s\");\n",
                          indent, decl->type, ctx, decl->name);
                  free(label);
                }
            }
          break;

      default:
        break;
    }
}

/*
 * For a single declaration, determine if we need a len, a subtree and an iter.
 * An array of opaques or strings needs only a length. A vector of opaques or
 * strings needs needs none of these (it is fixed lenght). An array of other
 * things needs a subtree, a length and an iterator, while a vector of other
 * things need a subtree and an iterator.
 *
 * Caller needs to ensure the bools are all correctly initialized.
 */
void get_extras_for_decl(declaration *decl, bool *needs_st, bool *needs_len,
                         bool *needs_iter)
{
  definition *new_def = NULL;
  relation rel = decl->rel;
  char *type = decl->type;

  new_def = resolve_type(decl->type, 0);

  if (new_def)
    {
      if (rel != REL_ARRAY && rel != REL_VECTOR)
        rel = new_def->def.ty.rel;
      type = new_def->def.ty.old_type;
    }

  d_fprintf(fout, "//decl_extras: N: %s, T: %s, rel = %d\n",
            decl->name, decl->type, rel);
  if (rel == REL_ARRAY || rel == REL_VECTOR)
    {
      if (strcmp(type, "opaque") == 0 ||
          strcmp(type, "string") == 0)
        {
          *needs_len = rel == REL_ARRAY;
          if (rel == REL_VECTOR)
            return;  /* We are done here */
        }

      /* Now, we know that we need a subtree, but do we need an iter? */
      *needs_len = rel == REL_ARRAY;
      *needs_st = true;
      if (strcmp(type, "opaque") != 0 &&
          strcmp(type, "string") != 0)
        *needs_iter = true;
    }
}

/*
 * For any declaration in the list, get all the extra things we need, like a len,
 * an iter and a subtree.
 */
static void get_extras_for_list(decl_list *list, bool *needs_st,
                         bool *needs_len, bool *needs_iter)
{
  struct decl_list *d = NULL;

  for (d = list; d != NULL; d = d->next)
    {
      get_extras_for_decl(&d->decl, needs_st, needs_len, needs_iter);

      /* if they are all set, we can go home, optimisation */
      if (*needs_st && *needs_len && *needs_iter)
        return;
    }
}

static void get_extras_for_union(struct case_list *list, bool *needs_st,
				bool *needs_len, bool *needs_iter)
{
  struct case_list *c = NULL;

  for (c = list; c != NULL; c = c->next)
    {
      if (c->contflag)
	continue;      /* We don't need anything for this */

       get_extras_for_decl(&c->case_decl, needs_st, needs_len, needs_iter);

       if (*needs_st && *needs_len && *needs_iter) /* Optimization */
         return;
    }
}

/*
 * Handle a self-referenctial structure, ie, a recursive one.
 *
 * TODO: This should be merged with the code for handling a non-recursive
 * structure.
 */
static void write_wireshark_recursive_struct_dissect(definition *prog,
                                                     definition *st)
{
  struct decl_list *d = NULL;
  char *ctx = (char *) "ist";
  bool needs_st = false, needs_len = false, needs_iter = false;

  get_extras_for_list(st->def.st.decls, &needs_st, &needs_len, &needs_iter);

  fprintf(fout, "%sint _U_\ndissect_%s(tvbuff_t *tvb, packet_info *pinfo _U_, "
                "proto_tree *tree, int offset, const char *label)\n",
          (prog->def.pr.prog_num ? "static " : ""), st->def_name);
  fprintf(fout, "{\n");
  fprintf(fout, "\tproto_tree *subtree = NULL;\n"
                "\tproto_item *it = NULL;\n"
                "\tint old_offset = offset;\n");
  fprintf(fout, "\tguint value_follows;\n");
  fprintf(fout, "\n");

  /* Insert a subtree named by this structure name */
  fprintf(fout, "\tsubtree = proto_tree_add_subtree(tree, tvb, offset, -1, "
                "ett_%s, &it, label);\n",
          st->def_name);
  /*
   * Each of these consists of a ValueFollows entry followed by the rest of
   * the structure, with the last field (the pointer) kind of beloning to
   * the next instance. So, insert code to fetch the 'value follows field',
   * add it to the subtree and then decode the struct if it follows.
   */
  fprintf(fout, "\tvalue_follows = tvb_get_ntohl(tvb, offset);\n");
  fprintf(fout, "\twhile (1) {\n");
  fprintf(fout, "\t\tproto_tree *ist = NULL;\n"
                "\t\tproto_item *iit = NULL;\n"
                "\t\tint ioo = offset;\n\n");
  fprintf(fout, "\t\tist = proto_tree_add_subtree(subtree, tvb, offset, -1, "
                "ett_%s_item, &iit, \"*%s\");\n\n",
          st->def_name, st->def_name);
  fprintf(fout, "\t\tproto_tree_add_uint(ist, hf_value_follows, tvb, offset,"
                "4, value_follows);\n");
  fprintf(fout, "\t\toffset += 4;\n");

  fprintf(fout, "\t\tif (value_follows) {\n");
  if (needs_st)
    {
      fprintf(fout, "\t\t\tproto_tree *st_xtra = NULL;\n"
                    "\t\t\tproto_item *it_xtra = NULL;\n");
      fprintf(fout, "\t\t\tunsigned int old_offset_xtra = 0;\n");
    }
  if (needs_len)
    {
      fprintf(fout, "\t\t\tunsigned int len = 0;\n");
    }
  if (needs_iter)
    {
      fprintf(fout, "\t\t\tunsigned int i = 0;\n");
      if (!needs_st) /* Don't insert these twice */
        fprintf(fout, "\t\t\tunsigned int old_offset_xtra = 0;\n");
    }

  for (d = st->def.st.decls; d != NULL; d = d->next)
    {
      if (d->next) /* As long as it's not the last entry */
        write_wireshark_decl(&d->decl, st->def_name, ctx, "\t\t\t", true);

    }
  fprintf(fout, "\t\t\tproto_item_set_len(iit, offset - ioo);\n");
  fprintf(fout, "\t\t} else {\n"
                "\t\t\tbreak;\n"
                "\t\t}\n\n");
  fprintf(fout, "\t\tvalue_follows = tvb_get_ntohl(tvb, offset);\n");
  fprintf(fout, "\t}\n\n");

  /* Insert code to fix up the length of the byte stream covered by this */
  fprintf(fout, "\n");
  fprintf(fout, "\tproto_item_set_len(it, offset - old_offset);\n");
  fprintf(fout, "\n\n\treturn offset;\n}\n\n");
}

static void write_wireshark_struct_dissect(definition *prog, definition *st)
{
  struct decl_list *d = NULL;
  char *ctx = (char *) "subtree";
  bool needs_st = false, needs_len = false, needs_iter = false;

  /*
   * If it is a structure that uses tail recursion, handle it differently.
   */
  if (st->def.st.self_pointer) {
    write_wireshark_recursive_struct_dissect(prog, st);
    return;
  }

  get_extras_for_list(st->def.st.decls, &needs_st, &needs_len, &needs_iter);

  fprintf(fout, "%sint _U_\ndissect_%s(tvbuff_t *tvb, packet_info *pinfo _U_, "
                "proto_tree *tree, int offset, const char *label)\n",
          (prog->def.pr.prog_num ? "static " : ""), st->def_name);
  fprintf(fout, "{\n");
  fprintf(fout, "\tproto_tree *subtree = NULL;\n"
                "\tproto_item *it = NULL;\n"
                "\tint old_offset = offset;\n");

  if (needs_st)
    {
      fprintf(fout, "\tproto_tree *st_xtra = NULL;\n"
                    "\tproto_item *it_xtra = NULL;\n");
      fprintf(fout, "\tunsigned int old_offset_xtra = 0;\n");
    }
  if (needs_len)
    {
      fprintf(fout, "\tunsigned int len = 0;\n");
    }
  if (needs_iter)
    {
      fprintf(fout, "\tunsigned int i = 0;\n");
      if (!needs_st) /* Don't insert these twice */
        fprintf(fout, "\tunsigned int old_offset_xtra = 0;\n");
    }

  fprintf(fout, "\n");

  /* Insert a subtree named by this structure name */
  fprintf(fout, "\tsubtree = proto_tree_add_subtree(tree, tvb, offset, -1, "
          "ett_%s, &it, label);\n",
          st->def_name);
  for (d = st->def.st.decls; d != NULL; d = d->next)
    {
      write_wireshark_decl(&d->decl, st->def_name, ctx, "\t", true);
    }
  /* Insert code to fix up the length of the byte stream covered by this */
  fprintf(fout, "\n");
  fprintf(fout, "\tproto_item_set_len(it, offset - old_offset);\n");
  fprintf(fout, "\n\n\treturn offset;\n}\n\n");
}

/*
 * Insert code to dissect the union type. Fetch the discriminant and
 * then hande each of the arms of the union.
 */
static void write_wireshark_union_dissect(definition *prog, definition *un)
{
  case_list *cases = un->def.un.cases;
  char ctx[1024]; /* HACK. FIXME */
  bool needs_st = false, needs_len = false, needs_iter = false;

  d_fprintf(fout, "//UN: name = %s, decl_name = %s, decl_type = %s\n",
            un->def_name, un->def.un.enum_decl.name, un->def.un.enum_decl.type);
  snprintf(ctx, sizeof(ctx), "%s_tree", un->def_name);
  fprintf(fout, "%sint _U_\ndissect_%s(tvbuff_t *tvb, packet_info *pinfo _U_, "
		"proto_tree *tree, int offset, const char *label)\n",
	  prog->def.pr.prog_num ? "static " : "", un->def_name);
  fprintf(fout, "{\n");
  fprintf(fout, "\tproto_item *%s_item;\n", un->def_name);
  fprintf(fout, "\tproto_tree *%s_tree;\n", un->def_name);
  fprintf(fout, "\tint old_offset = offset;\n");
  fprintf(fout, "\tguint32 %s;\n", un->def.un.enum_decl.name);

  get_extras_for_union(un->def.un.cases, &needs_st, &needs_len, &needs_iter);
  if (un->def.un.default_decl) /* Only if there is something there */
    get_extras_for_decl(un->def.un.default_decl, &needs_st, &needs_len, &needs_iter);
  if (needs_st)
    {
      fprintf(fout, "\tproto_tree *st_xtra = NULL;\n"
                    "\tproto_item *it_xtra = NULL;\n"
                    "\tint old_offset_xtra = 0;\n");
    }
  if (needs_iter)
    {
      fprintf(fout, "\tunsigned int i = 0;\n");
    }
  if (needs_len)
    {
      fprintf(fout, "\tunsigned int len = 0;\n");
    }


  fprintf(fout, "\n");

  fprintf(fout, "\t%s_tree = proto_tree_add_subtree(tree, tvb, offset, -1, "
  		"ett_%s, &%s_item, label);\n\n",
  	 un->def_name, un->def_name, un->def_name);
  fprintf(fout, "\tproto_tree_add_item_ret_uint(%s_tree, hf_%s, tvb, "
  		"offset, 4, ENC_BIG_ENDIAN, &%s);\n",
  	  un->def_name, un->def.un.enum_decl.type, un->def.un.enum_decl.name);
  fprintf(fout, "\toffset += 4;\n\n");

  fprintf(fout, "\tswitch (%s) {\n", un->def.un.enum_decl.name);

  /* Now handle the cases */
  while (cases) {
    d_fprintf(fout, "// CASE: name = %s, contflag = %d\n", cases->case_name,
              cases->contflag);
    fprintf(fout, "\t\tcase %s:\n", cases->case_name);
    if (!cases->contflag)
      write_wireshark_decl(&cases->case_decl, un->def_name, ctx, "\t\t", true);
    fprintf(fout, "\t\tbreak;\n");
    cases = cases->next;
  }

  /* Now the default */
  fprintf(fout, "\t\tdefault:\n");
  if (un->def.un.default_decl) {
    write_wireshark_decl(un->def.un.default_decl, un->def_name, ctx, "\t\t", true);
  }

  fprintf(fout, "\t\tbreak;\n");
  fprintf(fout, "\t}\n\n");

  fprintf(fout, "\tproto_item_set_len(%s_item, offset - old_offset);\n\n",
	  un->def_name);

  fprintf(fout, "\treturn offset;\n");

  fprintf(fout, "}\n\n");
}

/*
 * Write a single typedef definition. We know we need to generate it here
 * and that it needs to be non-static (is this true ...)
 *
 * Insert the definition, and a subtree, and then code to fetch the count,
 * insert it and handle the children.
 *
 * We only handle typedefs that are structs etc here for the moment since
 * we can pass in simple typedefs.
 */
static void write_wireshark_typedef_dissect(definition *def, definition *prog)
{
  definition *target;

  assert(def != NULL && def->def_kind == DEF_TYPEDEF);

  target = resolve_type(def->def.ty.old_type, 1);

  d_fprintf(fout, "//TY: name = %s, old_type = %s, rel = %d\n",
            def->def_name, def->def.ty.old_type, def->def.ty.rel);
  d_fprintf(fout, "//Target: %p\n", (void *)target);
  if (target == NULL ||
      (target->def_kind != DEF_STRUCT || def->def.ty.rel != REL_ARRAY))
    {
      d_fprintf(fout, "//Unknown or incorrect typedef target type: %s\n",
                     def->def.ty.old_type);
      return;
    }

  fprintf(fout, "%sint _U_\ndissect_%s(tvbuff_t *tvb, packet_info *pinfo _U_, "
		"proto_tree *tree, int offset, const char *label)\n",
	  prog->def.pr.prog_num ? "static " : "", def->def_name);
  fprintf(fout, "{\n");
  fprintf(fout, "\tproto_tree *subtree;\n");
  fprintf(fout, "\tproto_item *it;\n");
  fprintf(fout, "\tint old_offset = offset;\n");
  fprintf(fout, "\tint len = 0;\n");
  fprintf(fout, "\tint lab_len = 0;\n\tint log_len = 0;\n");
  fprintf(fout, "\tint i = 0;\n");

  /* Now, add the subtree */
  fprintf(fout, "\tsubtree = proto_tree_add_subtree_format(tree, "
                "tvb, offset, -1, ett_%s, &it, \"%%s<>\", label);\n\n",
                def->def_name);

  /* Now, insert the length and etc. */
  fprintf(fout, "\tlen = tvb_get_ntohl(tvb, offset);\n");
  fprintf(fout, "\tproto_tree_add_uint(subtree, hf_%s_len, tvb, "
                "offset, 4, len);\n", def->def_name);
  fprintf(fout, "\toffset += 4;\n\n");
  fprintf(fout, "\tlab_len = strlen(label);\n");
  fprintf(fout, "\tlog_len = log10(len) + 1;\n");

  /* Now handle the ones we need to iterate across */
  fprintf(fout, "\tfor (i = 0; i < len; i++) {\n");

  // Insert code to generate the label from the label passed in
  fprintf(fout, "\t\tchar decl_label[lab_len + 1 + log_len + 1 + 1];\n\n");
  fprintf(fout, "\t\tsnprintf(decl_label, lab_len + log_len + 3, \"%%s[%%d]\", "
                              "label, i);\n");

  fprintf(fout, "\t\toffset = dissect_%s(tvb, pinfo, subtree, offset, "
                "decl_label);\n", def->def.ty.old_type);

  fprintf(fout, "\t};\n\n");

  fprintf(fout, "\tproto_item_set_len(it, offset - old_offset);\n\n");

  fprintf(fout, "\treturn offset;\n");

  fprintf(fout, "}\n\n");
}

/*
 * Generate dissection for typedefs if we need to. We only need to if
 * we do not have a program section, because we are exporting dissectors
 * and the caller does not know what the type is. That is, they do not know
 * that the typedef is a list or whatever. Generally, this is only an issue for
 * a typedef that is to a struct union, array of such, etc. Others we have
 * another hack for. Also, needs to add an ett.
 */
static void write_wireshark_typedefs_dissect(definition *prog)
{
  if (prog->def_kind == DEF_FAKE)
    {
      list *l;
      definition *def;

      for (l = defined; l != NULL; l = l->next)
        {
          def = (definition *) l->val;

          switch (def->def_kind)
          {
            case DEF_TYPEDEF:
              d_fprintf(fout, "//Generating dissectors for typedefs %s: OT: %s "
                              "%d\n", def->def_name, def->def.ty.old_type,
                              def->def.ty.rel);
              write_wireshark_typedef_dissect(def, prog);
              break;

            default:
              break;
          }
        }

    }
}

static void write_wireshark_req_rsp_version_dissect1(definition *prog,
                                                     version_list *vers,
                                                     proc_list *proc)
{
  struct decl_list *d;
  char *ctx = (char *) "tree";
  definition *ret_type;
  bool needs_st = false, needs_len = false, needs_iter = false;

  /*
   * Figure out if we need some more variables here ...
   */
  get_extras_for_list(proc->args.decls, &needs_st, &needs_len, &needs_iter);
  fprintf(fout, "static int\ndissect_%s%s_call(tvbuff_t *tvb _U_, "
                "packet_info *pinfo _U_,\n"
                "\t\tproto_tree *tree, void *data _U_)\n",
          proc->proc_name, vers->vers_num);
  fprintf(fout, "{\n");
  fprintf(fout, "\tint offset = 0;\n");
  d_fprintf(fout, "//arg_num %d, args: %s, needs_st = %d, needs_len: %d, "
                  "needs_iter: %d\n", proc->arg_num,
            proc->args.argname, needs_st, needs_len, needs_iter);

  if (needs_st)
    {
      fprintf(fout, "\tproto_tree *st_xtra = NULL;\n"
                    "\tproto_item *it_xtra = NULL;\n"
                    "\tint old_offset_xtra = 0;\n");
    }
  if (needs_len)
    {
      fprintf(fout, "\tunsigned int len = 0;\n");
    }
  if (needs_iter)
    {
      fprintf(fout, "\tunsigned int i = 0;\n");
    }

  fprintf(fout, "\n");

  d_fprintf(fout, "//args_type: %s\n", proc->args.decls->decl.type);
  for (d = proc->args.decls; d != NULL; d = d->next)
    {
      d_fprintf(fout, "//args: N=%s, T=%s\n", d->decl.name, d->decl.type);
      write_wireshark_decl(&d->decl, proc->args.argname, ctx, "\t", true);
    }
  fprintf(fout, "\n");
  fprintf(fout, "\tproto_item_append_text(tree, \", %s Call\");\n",
          proc->proc_name);
  fprintf(fout, "\n\treturn offset;\n");
  fprintf(fout, "}\n\n");

  fprintf(fout, "static int\ndissect_%s%s_reply(tvbuff_t *tvb _U_, "
                "packet_info *pinfo _U_,\n"
                "\t\tproto_tree *tree, void *data _U_)\n",
          proc->proc_name, vers->vers_num);
  fprintf(fout, "{\n");
  fprintf(fout, "\tint offset = 0;\n\n");

  ctx = "tree";
  d_fprintf(fout, "//res_type: %s\n", proc->res_type);
  ret_type = resolve_type(proc->res_type, TRUE);
  if (ret_type)
    {
      switch (ret_type->def_kind)
        {
          case DEF_STRUCT:
            fprintf(fout, "\toffset = dissect_%s(tvb, pinfo, %s, "
                          "offset, \"%s\");\n",
                    ret_type->def_name, ctx, proc->res_type);
            break;

          case DEF_UNION:
            fprintf(fout, "\toffset = dissect_%s(tvb, pinfo, %s, "
                          "offset, \"%s\");\n",
                    ret_type->def_name, ctx, proc->res_type);
            break;

          case DEF_ENUM:
            fprintf(fout, "\tproto_tree_add_item(%s, hf_%s, tvb, "
  		          "offset, 4, ENC_BIG_ENDIAN);\n",
  	            ctx, proc->res_type);
            fprintf(fout, "\toffset += 4;\n\n");
            break;

          default:
            break;
        }
    }
  else
    {
      struct declaration new_decl;

      new_decl.name      = proc->proc_name;
      new_decl.prefix    = proc->res_prefix;
      new_decl.type      = proc->res_type;
      new_decl.rel       = REL_ALIAS;
      new_decl.array_max = "~0";
      d_fprintf(fout, "//non resolved type: %s, %s\n",
                new_decl.name, new_decl.type);
      write_wireshark_decl(&new_decl, proc->res_type, ctx, "\t", true);
    }
  fprintf(fout, "\n");
  fprintf(fout, "\tproto_item_append_text(tree, \", %s Reply\");\n",
          proc->proc_name);
  fprintf(fout, "\n\treturn offset;\n");
  fprintf(fout, "}\n\n");
}

static void write_wireshark_req_rsp_version_dissect(definition *prog,
                                                    version_list *vers)
{
  proc_list *p;

  for (p = vers->procs; p != NULL; p = p->next)
    {
      write_wireshark_req_rsp_version_dissect1 (prog, vers, p);
    }
}

static void write_wireshark_req_rsp_dissect(definition *prog)
{

  if (prog)
    {
      list *l;

      for (l = defined; l != NULL; l = l->next)
        {
          definition *def = NULL;
          def = (definition *) l->val;
          switch (def->def_kind)
            {
              version_list *v;

              case DEF_PROGRAM:
                for (v = def->def.pr.versions; v != NULL; v = v->next)
                  {
                    write_wireshark_req_rsp_version_dissect (prog, v);
                  }
              break;

              default:
              break;
            }
        }
  }
}

static void write_wireshark_proc_vals(definition *prog, version_list *vers)
{
  struct proc_list *p;
  fprintf(fout, "static const value_string %s%s_proc_vals[] = {\n",
          prog->def_name, vers->vers_num);
  for (p = vers->procs; p != NULL; p = p->next)
    {
      fprintf(fout, "\t{ %s, \"%s\" },\n",
              p->proc_num, p->proc_name);
    }
  fprintf(fout, "\t{ 0, NULL }\n};\n\n");
}

#define if_hf_name_not_printed(...) \
  hf_key = calloc(sizeof(struct hf_tree_info), 1); \
  snprintf(hf_key->name, sizeof(hf_key->name), __VA_ARGS__); \
  hf_node = tfind((void *)hf_key, &hf_tree_root, compare_hf_nodes); \
  if (!hf_node) { \
    fprintf(fout, "could not find node: %s\n", hf_key->name); \
    perror("node not found"); \
  } \
  hf_node = *(struct hf_tree_info **)hf_node; \
  d_fprintf(fout, "//Name: %s\n", hf_node->name); \
  if (!hf_node->printed)

static void write_wireshark_hf_decl_define (definition *def, declaration *decl)
{
  int len = 0, index = 0;
  definition *new_def = NULL;
  relation rel = decl->rel;
  const char *type = decl->type;
  const char *name = decl->name;
  struct hf_tree_info *hf_key = NULL;
  struct hf_tree_info *hf_node = NULL;

  new_def = resolve_type(decl->type, 0);

  if (new_def)
    {
      d_fprintf(fout, "//newDefHF: old_type: %s, array_max: %s, re; %d\n",
                new_def->def.ty.old_type, new_def->def.ty.array_max,
                new_def->def.ty.rel);

      if (rel != REL_VECTOR && rel != REL_ARRAY)
        rel = new_def->def.ty.rel;
      type = new_def->def.ty.old_type;
    }


  /* The following is already handled */
  if (strcmp(type, "void") == 0)
    return;

  if (rel == REL_VECTOR)
    {
      /* opaque and string have value hf fields. */
      if (strcmp(type, "opaque") == 0 ||
          strcmp(type, "string") == 0)
        {
          if_hf_name_not_printed("hf_%s_%s_val", def->def_name, decl->name)
            {
              fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
              fprintf(fout, "\t\t\t\"%s_val\", \"%s.%s_val\", %s, BASE_NONE,\n",
                      name, def->def_name, name,
                      strcmp(type, "opaque") ? "FT_STRING" : "FT_BYTES");
              fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL}},\n\n");
              hf_node->printed = 1;
            }
        }
    }
  else if (rel == REL_ARRAY)
    {
      if_hf_name_not_printed("hf_%s_%s_len", def->def_name, decl->name)
        {
          fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
          fprintf(fout, "\t\t\t\"%s_len\", \"%s.%s_len\", FT_UINT32, BASE_DEC,\n",
                  name, def->def_name, name);
          fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL }},\n\n");
          hf_node->printed = 1;
        }

      /*
       * Check if it's a base type or opaque or string do the correct thing.
       * If it's not, we don't need an hf field for the value
       * as the dissector takes care of that.
       */
      if (strcmp(type, "opaque") == 0 ||
          strcmp(type, "string") == 0)
        {
          if_hf_name_not_printed("hf_%s_%s_val", def->def_name, decl->name)
            {
              fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
              fprintf(fout, "\t\t\t\"%s_val\", \"%s.%s_val\", %s, BASE_NONE,\n",
                      name, def->def_name, name,
                      strcmp(type, "opaque") ? "FT_STRING" : "FT_BYTES");
              fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL}},\n\n");
              hf_node->printed = 1;
            }
        }
      else if (is_base_type(type, &len, &index))
        {
          if_hf_name_not_printed("hf_%s_%s_val", def->def_name, decl->name)
            {
              fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
              fprintf(fout, "\t\t\t\"%s_val\", \"%s.%s_val\", %s, BASE_DEC,\n",
                      name, def->def_name, name, rpc_types[index]);
              fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL}},\n\n");
              hf_node->printed = 1;
            }
        }
    }
  else if (rel == REL_POINTER)
    {
      if_hf_name_not_printed("hf_value_follows")
        {
          fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
          fprintf(fout, "\t\t\t\"value_follows\", \"%s.value_follows\", FT_UINT32, BASE_DEC,\n",
                   def->def_name);
           fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL }},\n\n");
           hf_node->printed = 1;
        }
    }
  else if (is_base_type(decl->type, &len, &index))
    {
      /* It's an alias ... so handle correctly ... */
      if_hf_name_not_printed("hf_%s_%s", def->def_name, decl->name)
        {
          fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
          fprintf(fout, "\t\t\t\"%s\", \"%s.%s\", %s, BASE_DEC,\n",
                  decl->name, def->def_name, decl->name, rpc_types[index]);
          fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL }},\n\n");
          hf_node->printed = 1;
        }
    }
  free(hf_key);
}

/*
 * Handle an ett entry visit. Check if it is an ett name and if we have
 * printed it already. If we haven't printed it, do so.
 *
 * We don't care about any order. As soon as we see the node deal with it.
 */
static void ett_visit(const void *nodep, const VISIT which, const int depth)
{
  struct hf_tree_info *node;

  if (which != leaf && which != postorder)
    return;

  node = *(struct hf_tree_info **)nodep;
  if (strncmp("ett_", node->name, 4) == 0)
    {
      if (!node->printed)
        {
          node->printed = 1;
          fprintf(fout, "\t\t&%s,\n", node->name);
        }
    }
}

/*
 * Add the code to register the hf_fields, etts, etc
 */
static void write_wireshark_register_arrays (definition *prog)
{
  if (prog)
    {
      char *progname_lower = NULL;
      int i;
      list *l;
      version_list *v;
      struct hf_tree_info *hf_key = NULL;
      struct hf_tree_info *hf_node = NULL;

      /*
       * Only want the program dissectors if this is non-fake
       */
      d_fprintf(fout, "//prog_num: %s, def_kind: %d\n", 
                      prog->def.pr.prog_num, prog->def_kind);
      if (prog->def.pr.prog_num && (prog->def_kind != DEF_FAKE))
        {
          for (v = prog->def.pr.versions; v != NULL; v = v->next)
            {
              write_wireshark_proc_vals(prog, v);
            }
        }
      fprintf(fout, "void proto_register_%s(void)\n", prog->def_name);
      fprintf(fout, "{\n");
      // Generate the hf fields for the procedure versions first.
      fprintf(fout, "\tstatic hf_register_info hf[] = {\n");

      if (void_needed)
        fprintf(fout, "\t\t{ &hf_void, {\n"
                      "\t\t\t\"void\", \"void\", FT_STRING, BASE_NONE,\n"
                      "\t\t\tNULL, 0, NULL, HFILL }},\n\n");

      for (l = defined; l != NULL; l = l->next)
        {
          definition *def;
          decl_list *d = NULL;
          case_list *c = NULL;
          version_list *v = NULL;
          int len = 0, index = 0;
          definition *target;

          def = (definition *) l->val;
          switch (def->def_kind)
            {
              case DEF_STRUCT:
                for (d = def->def.st.decls; d != NULL; d = d->next)
                  {
                    write_wireshark_hf_decl_define(def, &d->decl);
                  }
                break;

              case DEF_UNION:
                if_hf_name_not_printed("hf_%s", def->def.un.enum_decl.type)
                  {
                    fprintf(fout, "\t\t{ &%s, {\n"
                                  "\t\t\t\"%s\", \"%s\", FT_UINT32, BASE_DEC,\n",
                            hf_key->name, def->def_name, def->def_name);

                    /* HACK: if a bool, use int, otherwise the vals string. */
                    if (strcmp(def->def.un.enum_decl.type, "bool") == 0)
                      fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL }},\n\n");
                    else
                      fprintf(fout, "\t\t\t&%s_vals, 0, NULL, HFILL }},\n\n",
                              def->def.un.enum_decl.type);
                    hf_node->printed = 1;
                  }
                if (def->def.un.default_decl) /* Only if it exists */
                  write_wireshark_hf_decl_define(def, def->def.un.default_decl);
                for (c = def->def.un.cases; c != NULL; c = c->next)
                  if (!c->contflag) /* If it is a continuation, nothing to do */
                    write_wireshark_hf_decl_define(def, &c->case_decl);
                break;

              case DEF_TYPEDEF:
                /*
                 * For those typedefs that map to a list we need one of these
                 */
                target = resolve_type(def->def.ty.old_type, 1);
                if (prog->def_kind == DEF_FAKE &&
                    target != NULL && target->def_kind == DEF_STRUCT &&
                    def->def.ty.rel == REL_ARRAY)
                  {
                    if_hf_name_not_printed("hf_%s_len", def->def_name)
                      {
                        fprintf(fout, "\t\t{ &%s, {\n", hf_key->name);
                        fprintf(fout, "\t\t\t\"%s_len\", \"%s_len\", FT_UINT32, "
                                      "BASE_DEC,\n",
                                      def->def_name, def->def_name);
                        fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL }},\n\n");
                        hf_node->printed = 1;
                      }
                  }
                break;

              case DEF_ENUM:
                if_hf_name_not_printed("hf_%s", def->def_name)
                  {
                    fprintf(fout, "\t\t{ &%s, {\n"
                                  "\t\t\t\"%s\", \"%s\", FT_UINT32, BASE_DEC,\n"
                                  "\t\t\t&%s_vals, 0, NULL, HFILL }},\n\n",
                            hf_key->name, def->def_name, def->def_name,
                            def->def_name);
                    hf_node->printed = 1;
                  }
                break;

              case DEF_PROGRAM:
                /* Don't need this if not asked for */
                if (prog->def_kind == DEF_FAKE)
                  break;
                for (v = def->def.pr.versions; v != NULL; v = v->next)
                  {
                    proc_list *p = NULL;
                    fprintf(fout, "\t\t{ &hf_%s_procedure_v%s, {\n",
                            prog->def_name, v->vers_num);
                    fprintf(fout, "\t\t\t\"V%s Procedure\", \"%s.procedure_v%s\", "
                                  "FT_UINT32, BASE_DEC,\n",
                            v->vers_num, prog->def_name, v->vers_num);
                    fprintf(fout, "\t\t\tVALS(%s%s_proc_vals), 0, NULL, HFILL }},\n\n",
                            prog->def_name, v->vers_num);

                    /* Now the args of each proc */
                    for (p = v->procs; p != NULL; p = p->next)
                      {
                        /*
                         * Now the return type ... might only have to worry about
                         * base types. Reject void as well.
                         */
                        if (strcmp(p->res_type, "void") != 0 &&
                            is_base_type(p->res_type, &len, &index))
                          {
                            fprintf(fout, "\t\t{ &hf_%s_%s, {\n",
                                    p->proc_name, p->res_type);
                            fprintf(fout, "\t\t\t\"%s\", \"%s.%s\", %s, BASE_DEC,\n",
                                    p->proc_name, p->proc_name, p->res_type,
                                    rpc_types[index]);
                            fprintf(fout, "\t\t\tNULL, 0, NULL, HFILL}},\n\n");
                          }

                        /* And now the args ... */
                        for (d = p->args.decls; d != NULL; d = d->next)
                          {
                            definition def;
                            def.def_kind = DEF_STRUCT;
                            def.def_name = p->args.argname;

                            write_wireshark_hf_decl_define(&def, &d->decl);
                          }
                      }
                  }
                break;

              default:
                break;
            }
          }

      fprintf(fout, "\t};\n\n");

      fprintf(fout, "\tstatic gint *ett[] = {\n");

      twalk(hf_tree_root, ett_visit);

      fprintf(fout, "\t};\n\n");

      progname_lower = calloc(1, strlen(prog->def_name) + 1);
      for (i = 0; i < strlen(prog->def_name); i++)
        progname_lower[i] = tolower(prog->def_name[i]);
      fprintf(fout, "\tproto_%s = proto_register_protocol(\"%s\", \"%s\", "
                    "\"%s\");\n",
              prog->def_name,
              protocol_description == NULL ? prog->def_name : protocol_description,
              prog->def_name, progname_lower);
      fprintf(fout, "\tproto_register_field_array(proto_%s, hf, "
                    "array_length(hf));\n",
              prog->def_name);
      fprintf(fout, "\tproto_register_subtree_array(ett, array_length(ett));\n");
      fprintf(fout, "}\n\n");
    }
}

// Insert the vsff table for a specific version
static void write_wireshark_version (definition *prog, version_list *vers)
{
  struct proc_list *p;

  fprintf(fout, "static const vsff %s_v%s_proc[] = {\n",
          prog->def_name, vers->vers_num);
  for (p = vers->procs; p != NULL; p = p->next)
    {
      fprintf(fout, "\t{ %s, \"%s\", dissect_%s%s_call, dissect_%s%s_reply },\n",
              p->proc_num, p->proc_name, p->proc_name, vers->vers_num,
              p->proc_name, vers->vers_num);
    }
  fprintf(fout, "\t{ 0, NULL, NULL, NULL }\n};\n\n");
}

/*
 * Only generate this part if we have a program number.
 */
static void write_wireshark_trailer (definition *prog)
{
  if (prog && prog->def.pr.prog_num && !(prog->def_kind == DEF_FAKE))
    {
      struct version_list *v;
      // Generate per-version proc info arrays
      for (v = prog->def.pr.versions; v != NULL; v = v->next)
        {
          write_wireshark_version(prog, v);
        }

      // Generate version info array
      fprintf(fout, "static const rpc_prog_vers_info %s_vers_info[] = {\n",
              prog->def_name);
      for (v = prog->def.pr.versions; v != NULL; v = v->next)
        {
          fprintf(fout, "\t{ %s, %s_v%s_proc, &hf_%s_procedure_v%s },\n",
                  v->vers_num, prog->def_name, v->vers_num,
                  prog->def_name, v->vers_num);
        }
      fprintf(fout, "};\n\n");

      // Now, the registration
      fprintf(fout, "void\nproto_reg_handoff_%s(void)\n", prog->def_name);
      fprintf(fout, "{\n\trpc_init_prog(proto_%s, %s, ett_%s,\n"
                    "\t    G_N_ELEMENTS(%s_vers_info), %s_vers_info);\n",
              prog->def_name, prog->def.pr.prog_num, prog->def_name,
              prog->def_name, prog->def_name);
      fprintf(fout, "}\n");
    }
}
