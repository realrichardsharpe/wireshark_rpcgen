/*      @(#)rpc_wireshark.h  1.0  16/09/01
 *
 * Copyright to be determined ...
 *
 * rpc_wireshark.h, Definitions for the generation of a wireshark
 * dissector in rpcgen
 */

struct typedef_list {
	struct definition *def;
	struct typedef_list *next;
};
typedef struct typedef_list typedef_list;

struct enum_list {
	struct definition *def;
        struct enum_list *next;
};
typedef struct enum_list enum_list;

void write_wireshark (const char *infilename);
